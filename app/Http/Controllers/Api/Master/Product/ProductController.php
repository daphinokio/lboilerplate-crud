<?php

namespace App\Http\Controllers\Api\Master\Product;

use App\Events\Backend\Master\Product\ProductDeleted;
use App\Http\Controllers\Api\Master\BaseController;
use App\Http\Requests\Api\Master\Product\ManageProductRequest;
use App\Http\Requests\Api\Master\Product\StoreProductRequest;
use App\Http\Requests\Api\Master\Product\UpdateProductRequest;
use App\Models\Master\Product;
use App\Models\Master\Vendor;
use App\Repositories\Backend\Master\ProductRepository;
use App\Repositories\Backend\Master\VendorRepository;
use Illuminate\Http\Request;

class ProductController extends BaseController
{
    protected $productRepository;
    protected $vendorRepository;

    public function __construct(ProductRepository $productRepository,
                                VendorRepository $vendorRepository)
    {
        $this->productRepository = $productRepository;
        $this->vendorRepository = $vendorRepository;
    }

    public function index(ManageProductRequest $request)
    {
        if ($request->all())
        {
            return $this->resultJson(
                $this->productRepository->getByTag($request->tag),
                200,
                'OK'
            );
        }

        return $this->resultJson(
            $this->productRepository
                ->with('tags', 'vendor')
                ->orderBy('id')
                ->paginate(25),
            200,
            'OK'
        );
    }

    public function show(ManageProductRequest $request, Product $product)
    {
        $product->vendor;
        $product->tags;
        return $this->resultJson($product, 200, 'OK');
    }

    public function store(StoreProductRequest $request)
    {
        $vendor = $this->vendorRepository->getById($request->vendor_id);

        $product = $this->productRepository->store(
            $vendor,
            $request->only('name', 'price', 'tags')
        );

        if ($product)
        {
            $product->tags;
            return $this->resultJson(
                $product,
                200,
                'OK'
            );
        }
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        $product = $this->productRepository->update(
            $product,
            $request->only('name', 'price', 'tags', 'vendor_id')
        );

        if ($product)
            return $this->resultJson($product, 200, 'OK');
    }

    public function delete(ManageProductRequest $request, Product $product)
    {
        $this->productRepository->deleteById($product->id);

        event(new ProductDeleted($product));

        return $this->resultJson(
            null,
            200,
            'OK'
        );
    }
}
