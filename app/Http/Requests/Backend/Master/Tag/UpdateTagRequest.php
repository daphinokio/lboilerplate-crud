<?php


namespace App\Http\Requests\Backend\Master\Tag;


use Illuminate\Foundation\Http\FormRequest;

class UpdateTagRequest extends FormRequest
{

    public function authorize()
    {
        return $this->user()->isAdmin();
    }


    public function rules()
    {
        return [
            'name' => ['required']
        ];
    }
}
