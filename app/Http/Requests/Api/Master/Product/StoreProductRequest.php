<?php


namespace App\Http\Requests\Api\Master\Product;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreProductRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => ['required', Rule::unique('products')],
            'price' => ['required', 'integer'],
            'vendor_id' => ['required', 'integer']
        ];
    }
}
