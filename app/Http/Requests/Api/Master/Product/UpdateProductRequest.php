<?php


namespace App\Http\Requests\Api\Master\Product;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProductRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => ['required'],
            'price' => ['required', 'integer'],
            'vendor_id' => ['required', 'integer']
        ];
    }
}
