<?php


namespace App\Http\Requests\Api\Master\Vendor;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateVendorRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => ['required'],
            'address' => ['required']
        ];
    }
}
