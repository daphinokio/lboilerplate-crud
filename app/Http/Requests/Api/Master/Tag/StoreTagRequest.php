<?php


namespace App\Http\Requests\Api\Master\Tag;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreTagRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => ['required', Rule::unique('tags')]
        ];
    }
}
