<?php

namespace App\Listeners\Backend\Master\Vendor;

use App\Events\Backend\Master\Vendor\VendorUpdated;
use App\Events\Backend\Master\Vendor\VendorDeleted;
use App\Events\Backend\Master\Vendor\VendorCreated;

class VendorEventListener
{
    public function onCreated($event)
    {
        logger('Vendor Created');
    }

    public function onUpdated($event)
    {
        logger('Vendor Updated');
    }

    public function onDeleted($event)
    {
        logger('Vendpr Deleted');
    }

    public function subscribe($events)
    {
        $events->listen(
            VendorUpdated::class,
            'App\Listeners\Backend\Master\Vendor\VendorEventListener@onUpdated'
        );

        $events->listen(
            VendorDeleted::class,
            'App\Listeners\Backend\Master\Vendor\VendorEventListener@onDeleted'
        );

        $events->listen(
            VendorCreated::class,
            'App\Listeners\Backend\Master\Vendor\VendorEventListener@onCreated'
        );
    }
}
