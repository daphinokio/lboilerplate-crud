<?php

namespace App\Models\Master;

use App\Models\Master\Traits\Relationship\TagRelationship;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use TagRelationship;

    protected $guarded = [];

}
