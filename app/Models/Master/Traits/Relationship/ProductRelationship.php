<?php


namespace App\Models\Master\Traits\Relationship;


use App\Models\Master\Vendor;
use App\Models\Master\Tag;

trait ProductRelationship
{
    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }
}
