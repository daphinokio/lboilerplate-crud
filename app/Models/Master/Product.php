<?php

namespace App\Models\Master;

use App\Models\Master\Traits\Relationship\ProductRelationship;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use ProductRelationship;

    protected $fillable = ['name', 'price'];
}
