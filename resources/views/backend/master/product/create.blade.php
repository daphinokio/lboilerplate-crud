@extends('backend.layouts.app')

@section('title', 'Product Management | Create Product')

@section('content')
    {{ html()->form('POST', route('admin.master.product.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Product Managament
                        <small class="text-muted">Create Product</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4">
                <div class="col">
                    <div class="form-group row">
                        {{ html()->label('Name')
                            ->class('col-md-2 form-control-label')
                            ->for('name') }}

                        <div class="col-md-10">
                            {{ html()->text('name')
                                ->class('form-control')
                                ->placeholder('Name')
                                ->attribute('maxlength', 191)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label('Price')
                            ->class('col-md-2 form-control-label')
                            ->for('price') }}

                        <div class="col-md-10">
                            {{ html()->number('price')
                                ->class('form-control')
                                ->placeholder('Price')
                                ->attribute('maxlength', 191)
                                ->attribute('min', 0)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label('Vendor Name')
                            ->class('col-md-2 form-control-label')
                            ->for('vendor_id') }}

                        <div class="col-md-10">
                            <select name="vendor_id"
                                    id="vendor_id"
                                    required
                                    class="form-control">
                                <option value="">--Select Vendor Name--</option>
                                @foreach($vendors as $vendor)
                                    <option value="{{$vendor->id}}">{{ucwords($vendor->name)}}</option>
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label('Tags')
                            ->class('col-md-2 form-control-label')
                            ->for('tags') }}

                        <div class="col-md-10">
                            {{ html()->text('tags')
                                ->class('form-control')
                                ->placeholder('Tag1,Tag2,...')
                                ->required()}}
                            <small class="text-muted">Separate with comma (,)</small>
                        </div><!--col-->
                    </div><!--form-group-->


                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.auth.role.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
