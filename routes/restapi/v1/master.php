<?php

use App\Http\Controllers\Api\Master\Product\ProductController;
use App\Http\Controllers\Api\Master\Tag\TagController;
use App\Http\Controllers\Api\Master\Vendor\VendorController;

Route::group([
    'namespace' => 'Api\Master',
    'prefix' => 'master'
], function () {

    // Product Route
    Route::group(['namespace' => 'Product'], function() {

        Route::get('product', [ProductController::class, 'index']);
        Route::post('product', [ProductController::class, 'store']);

        Route::group(['prefix' => 'product/{product}'], function () {
            Route::get('', [ProductController::class, 'show']);
            Route::patch('/', [ProductController::class, 'update']);
            Route::delete('/', [ProductController::class, 'delete']);
        });

    });

    // Vendor Route
    Route::group(['namespace' => 'Vendor'], function() {

        Route::get('vendor', [VendorController::class, 'index']);
        Route::post('vendor', [VendorController::class, 'store']);

        Route::group(['prefix' => 'vendor/{vendor}'], function () {
            Route::patch('/', [VendorController::class, 'update']);
            Route::delete('/', [VendorController::class, 'delete']);
        });

    });


    // Tag Route
    Route::group(['namespace' => 'Tag'], function() {

        Route::get('tag', [TagController::class, 'index']);
        Route::post('tag', [TagController::class, 'store']);

        Route::group(['prefix' => 'tag/{tag}'], function () {
            Route::patch('/', [TagController::class, 'update']);
            Route::delete('/', [TagController::class, 'delete']);
        });

    });

});
