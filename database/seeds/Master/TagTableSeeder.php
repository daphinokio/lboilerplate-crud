<?php

use App\Models\Master\Tag;
use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        factory(Tag::class, 5)->create();

        $this->enableForeignKeys();
    }
}
