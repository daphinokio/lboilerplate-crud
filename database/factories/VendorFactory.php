<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Master\Vendor;
use Faker\Generator as Faker;

$factory->define(Vendor::class, function (Faker $faker) {
    return [
        'name' => strtolower($faker->name),
        'address' => $faker->address
    ];
});
